# frozen_string_literal: true

require 'gitlab'

# A helper class for wrapping the gitlab client
class GitlabClientHelper
  def initialize(private_token = '')
    endpoint = ENV.fetch('CI_API_V4_URL')
    @gitlab_client = Gitlab.client(endpoint:, private_token:)
  end

  def create_issue_note(project_id, issue_iid, message)
    gitlab_client.create_issue_note(project_id, issue_iid, message)
  end

  def issues(project_id, options = { state: 'opened' })
    options.merge!(per_page: 100)
    gitlab_client.issues(project_id, options).auto_paginate
  end

  def group_access_requests(group_id)
    gitlab_client.group_access_requests(group_id).auto_paginate
  end

  def repo_file(project_id, file_path)
    gitlab_client.repo_file_contents(project_id, file_path, 'main')
  end

  def create_issue(project_id, title, description)
    gitlab_client.create_issue(project_id, title, { description: })
  end

  def deny_group_access_request(group_id, user_id)
    gitlab_client.deny_group_access_request(group_id, user_id)
  end

  def group_members(group_id, options = {})
    options.merge!(per_page: 100)
    gitlab_client.group_members(group_id, options).auto_paginate
  end

  attr_reader :gitlab_client
end
