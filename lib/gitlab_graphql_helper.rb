# frozen_string_literal: true

require 'httparty'
require 'json'

# A helper class for calling the GitLab GraphQL API
class GitlabGraphqlHelper
  def initialize(token)
    @token = token
    @graphql_api_url = ENV.fetch('CI_API_GRAPHQL_URL')
  end

  def execute_graphql(query, max_attempts: 3)
    cookie_hash = HTTParty::CookieHash.new
    cookie_hash.add_cookies('gitlab_canary=true')

    attempts = 0

    begin
      attempts += 1

      response = HTTParty.post(
        @graphql_api_url,
        body: { query: }.to_json,
        headers: {
          'Content-Type' => 'application/json',
          'Cookie' => cookie_hash.to_cookie_string,
          'Authorization' => "Bearer #{@token}",
          'User-Agent' => '@gitlab-community/meta'
        }
      )

      raise "Received a non-default error code #{response.code} after #{attempts} attempts" if response.code != 200

      response.parsed_response
    rescue StandardError => e
      raise if attempts >= max_attempts

      puts e.message
      puts 'Retrying...'

      sleep_time = [2**(attempts - 1), 30].min + rand(0.1..0.5)
      sleep(sleep_time)
      retry
    end
  end
end
