# frozen_string_literal: true

require_relative '../../bin/access_requests'

RSpec.describe CommunityForksAccessRequests do
  before do
    stub_env('ACCESS_REQUESTS_GITLAB_API_TOKEN', 'glpat-xxxxxxxxxxxxxxxxxxxx')
    stub_env('CI_API_V4_URL', 'https://gitlab.com/api/v4')
    allow(Date).to receive(:today).and_return(Date.new(2024, 10, 28))
  end

  let(:instance) { described_class.new }

  describe '.onboarding_issues', :vcr do
    it 'returns expected issues' do
      result = instance.onboarding_issues

      expect(result.map(&:title)).to match_array(
        [
          '@trowton-alkami contributor onboarding',
          '@nathanpbrophy contributor onboarding',
          '@zadjadr contributor onboarding',
          '@dcoomber contributor onboarding',
          '@pongsakorn.reo contributor onboarding',
          '@asrkrasin contributor onboarding',
          '@NoobInTheNet contributor onboarding',
          '@SvenPistre contributor onboarding',
          '@ttekciteel contributor onboarding'
        ]
      )
    end
  end

  describe '.access_requests', :vcr do
    it 'returns expected access requests' do
      result = instance.access_requests

      expect(result.map(&:username)).to match_array(
        [
          '0xn3va',
          'Akhoriz',
          'SvenPistre',
          'asrkrasin',
          'ayushgupta11442',
          'gyury.lee9',
          'itj_jperez',
          'nihiu',
          'pongsakorn.reo',
          'ranjeetpatil3698',
          'ttekciteel',
          'zerr01'
        ]
      )
    end
  end

  describe '.issue_description', :vcr do
    it 'returns expected description' do
      result = instance.issue_description('lee')

      expect(result).to eq(
        <<~DESC
          <!-- Title: @username contributor onboarding -->

          /assign @lee
          /label onboarding
          /label access-request::pending

          ## Welcome to your GitLab contributor onboarding issue! :tada:

          Hi @lee, welcome to the GitLab community :wave:

          This issue is designed to help you get up and running as a GitLab contributor.
          One of the maintainers in the @gitlab-community/maintainers group will review your community forks access request
          and be available to help you with any onboarding questions.

          ### Onboarding steps

          :white_check_mark: We've created a checklist of contributor onboarding items, some of which you might have already completed!

          #### Get started

          - [ ] Do stuff!

          #### Learn the contribution process

          - [ ] Do stuff!

          #### Find an issue

          - [ ] Do stuff!

          #### Make your first contribution

          - [ ] Do stuff!

          #### Level up

          - [ ] Do stuff!
        DESC
      )
    end
  end

  describe '.issue_title' do
    it 'returns expected title' do
      result = instance.issue_title('lee')

      expect(result).to eq('@lee contributor onboarding')
    end
  end

  describe '.create_new_onboarding_issues' do
    let(:gitlab_client) { instance_double(GitlabClientHelper) }

    before do
      allow(GitlabClientHelper).to receive(:new).and_return(gitlab_client)
      allow(gitlab_client).to receive(:group_access_requests)
        .with(described_class::COMMUNITY_FORK_MEMBERS_GROUP_ID)
        .and_return(
          [
            double(Gitlab::ObjectifiedHash, username: 'alread-created', requested_at: Date.today.iso8601),
            double(Gitlab::ObjectifiedHash, username: 'lee', requested_at: Date.today.iso8601),
            double(Gitlab::ObjectifiedHash, username: 'too-early', requested_at: (Date.today - 8).iso8601)
          ]
        )
      allow(gitlab_client).to receive(:issues)
        .with(described_class::ONBOARDING_PROJECT_ID, { created_after: (Date.today - 10).iso8601 })
        .and_return(
          [
            double(Gitlab::ObjectifiedHash, assignees: [double(Gitlab::ObjectifiedHash, username: 'alread-created')])
          ]
        )
      allow(gitlab_client).to receive(:repo_file).with(
        described_class::COMMUNITY_FORK_META_PROJECT_ID,
        '.gitlab/issue_templates/Onboarding.md'
      ).and_return('template')
    end

    context 'when dry run' do
      before do
        allow(instance).to receive(:puts)
      end

      it 'outputs the expected messages' do
        instance.create_new_onboarding_issues

        expect(instance).to have_received(:puts).with('Access request issue already exists for: alread-created')
        expect(instance).to have_received(:puts).with(
          'Access request for: too-early occurred more than 7 days ago'
        )
        expect(instance).to have_received(:puts).with(
          "Would create issue for lee with title: @lee contributor onboarding and description:\n```\ntemplate\n```"
        )
      end
    end

    context 'when not dry run' do
      before do
        stub_env('DRY_RUN', '0')
        allow(gitlab_client).to receive(:create_issue).with(
          described_class::ONBOARDING_PROJECT_ID,
          '@lee contributor onboarding',
          'template'
        ).and_return(double(Gitlab::ObjectifiedHash, iid: 1))
      end

      it 'creates the expected onboarding issues' do
        instance.create_new_onboarding_issues

        expect(gitlab_client).to have_received(:create_issue).with(
          described_class::ONBOARDING_PROJECT_ID,
          '@lee contributor onboarding',
          'template'
        )
      end
    end
  end
end
