# frozen_string_literal: true

require_relative '../../bin/sync_comment_templates'

RSpec.describe SyncCommentTemplates do
  before do
    stub_env('CI_API_GRAPHQL_URL', 'https://gitlab.com/api/graphql')
    stub_env('SYNC_COMMENT_TEMPLATES_GITLAB_API_TOKEN', 'glpat-xxxxxxxxxxxxxxxxxxxx')
    allow(Dir).to receive(:glob).with('.gitlab/comment_templates/60607268/*.md').and_return(
      [
        '.gitlab/comment_templates/60607268/Access request approval.md',
        '.gitlab/comment_templates/60607268/Access request information needed.md'
      ]
    )
    allow(File).to receive(:read).with('.gitlab/comment_templates/60607268/Access request approval.md')
      .and_return('ARA')
    allow(File).to receive(:read).with('.gitlab/comment_templates/60607268/Access request information needed.md')
      .and_return('ARIN')
  end

  let(:instance) { described_class.new(60_607_268, 'gitlab-community/community-members/onboarding') }

  describe '.execute', :vcr do
    context 'when unauthorized' do
      it 'raises an error' do
        expect do
          instance.execute
        end.to raise_error.with_message('Received a non-default error code 401 after 3 attempts')
      end
    end

    context 'when the repo does not match' do
      it 'raises an error' do
        expect { instance.execute }.to raise_error
      end

      context 'when REPORT_ONLY=1' do
        before do
          stub_env('REPORT_ONLY', '1')
        end

        it 'does not raise an error' do
          expect { instance.execute }.not_to raise_error
        end
      end

      context 'when FORCE_SYNC=1' do
        before do
          stub_env('FORCE_SYNC', '1')
        end

        it 'ouputs the expected updates' do
          allow(instance).to receive(:puts)

          instance.execute

          expect(instance).to have_received(:puts).with('Template matches: Access request approval')
          expect(instance).to have_received(:puts).with('Template matches: Access request information needed')
          expect(instance).to have_received(:puts).with("Disk template doesn't exist: Test2")
          expect(instance).to have_received(:puts).with('Would execute:').twice
          expect(instance).to have_received(:puts).with("mutation {\n  projectSavedReplyDestroy(input: {\n    id: \"gid://gitlab/Projects::SavedReply/215\"\n  }) {\n    errors\n  }\n}\n") # rubocop:disable Layout/LineLength
          expect(instance).to have_received(:puts).with("Disk template doesn't exist: Test")
          expect(instance).to have_received(:puts).with("mutation {\n  projectSavedReplyDestroy(input: {\n    id: \"gid://gitlab/Projects::SavedReply/214\"\n  }) {\n    errors\n  }\n}\n") # rubocop:disable Layout/LineLength
        end

        context 'when DRY_RUN=0' do
          before do
            stub_env('DRY_RUN', '0')
            allow(HTTParty).to receive(:post).and_call_original
          end

          it 'executes the expected GraphQL mutations' do
            instance.execute

            expect(HTTParty).to have_received(:post).with(
              'https://gitlab.com/api/graphql',
              hash_including(body: '{"query":"mutation {\n  projectSavedReplyDestroy(input: {\n    id: \"gid://gitlab/Projects::SavedReply/214\"\n  }) {\n    errors\n  }\n}\n"}') # rubocop:disable Layout/LineLength
            )
            expect(HTTParty).to have_received(:post).with(
              'https://gitlab.com/api/graphql',
              hash_including(body: '{"query":"mutation {\n  projectSavedReplyDestroy(input: {\n    id: \"gid://gitlab/Projects::SavedReply/215\"\n  }) {\n    errors\n  }\n}\n"}') # rubocop:disable Layout/LineLength
            )
          end
        end
      end
    end

    context 'when the repo does match', :vcr do
      it 'reports no mismatches' do
        expect { instance.execute }.not_to raise_error
      end
    end
  end
end
