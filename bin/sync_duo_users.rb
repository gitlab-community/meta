#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../lib/gitlab_graphql_helper'

require 'pry-byebug'

# A class for syncing gitlab-community/community-members
# with assigned GitLab Duo Pro seats
# rubocop:disable Metrics/ClassLength
class SyncDuoUsers
  ADDON_PURCHASE_ID = 'gid://gitlab/GitlabSubscriptions::AddOnPurchase/1002103'
  DUO_USERS_PATH = 'gitlab-community/community-members'
  ROOT_GROUP_PATH = 'gitlab-community'

  # The bot users we should probably use our bot gem for, but this is a quick fix
  EXCLUDE_USERS = %w[
    group_61840430_bot_cd45e2cdb0fb7ae0e2a22fe1da05cea5
    group_61840430_bot_05d2e891780d9496b3e99ef8dec34c11
    service_account_group_60717473_3f27c0b7cec844285a54381e5d91df38
    community-forks-access-requests
  ].freeze

  def initialize
    token = ENV.fetch('SYNC_DUO_USERS_GITLAB_API_TOKEN')
    @graphql_helper = GitlabGraphqlHelper.new(token)
    @force_sync = ENV.fetch('FORCE_SYNC', '0') == '1'
    @report_only = ENV.fetch('REPORT_ONLY', '0') == '1'
    @dry_run = ENV.fetch('DRY_RUN', '1') != '0'
  end

  def group_members_query(after)
    <<~QUERY
      query {
        group(fullPath: "#{DUO_USERS_PATH}") {
          groupMembers(
            relations: DIRECT
            after: "#{after}"
          ) {
            nodes {
              user {
                id
                username
              }
            }
            pageInfo {
              hasNextPage
              endCursor
            }
          }
        }
      }
    QUERY
  end

  def group_members
    users = []
    after = nil
    loop do
      result = @graphql_helper.execute_graphql(group_members_query(after))
        .dig('data', 'group', 'groupMembers')
      users.concat(
        result['nodes'].map { |user| { id: user['user']['id'], username: user['user']['username'] } }
      )

      return users unless result['pageInfo']['hasNextPage'] == true

      after = result['pageInfo']['endCursor']
      print '.'
    end
  end

  def add_user(id)
    query = <<~QUERY
      mutation {
        userAddOnAssignmentCreate(
          input: { userId: "#{id}", addOnPurchaseId: "#{ADDON_PURCHASE_ID}" }
        ) {
          errors
        }
      }
    QUERY

    execute_mutation(query)
  end

  def remove_user(id)
    query = <<~QUERY
      mutation {
        userAddOnAssignmentRemove(
          input: { userId: "#{id}", addOnPurchaseId: "#{ADDON_PURCHASE_ID}" }
        ) {
          errors
        }
      }
    QUERY

    execute_mutation(query)
  end

  def execute_mutation(query)
    if @dry_run
      puts 'Would execute:'
      puts query
      puts '==='
    else
      @graphql_helper.execute_graphql(query)
    end
  end

  def duo_seats
    users = []
    after = nil
    loop do
      result = @graphql_helper.execute_graphql(duo_seats_query(after))
        .dig('data', 'namespace', 'addOnEligibleUsers')
      users.concat(
        result['nodes']
          .select { |user| user['addOnAssignments']['nodes'].any? }
          .map { |user| { id: user['id'], username: user['username'] } }
      )

      return users unless result['pageInfo']['hasNextPage'] == true

      after = result['pageInfo']['endCursor']
      print '.'
    end
  end

  def duo_seats_query(after)
    <<~QUERY
      query {
        namespace(fullPath: "#{ROOT_GROUP_PATH}") {
          addOnEligibleUsers(
            addOnType: DUO_ENTERPRISE
            filterByAssignedSeat: "true"
            addOnPurchaseIds: ["#{ADDON_PURCHASE_ID}"]
            after: "#{after}"
          ) {
            nodes {
              id
              username
              addOnAssignments(addOnPurchaseIds: ["#{ADDON_PURCHASE_ID}"]) {
                nodes {
                  addOnPurchase {
                    name
                  }
                }
              }
            }
            pageInfo {
              hasNextPage
              endCursor
            }
          }
        }
      }
    QUERY
  end

  def execute
    puts 'Fetching group members...'
    members = group_members.reject { |user| EXCLUDE_USERS.include?(user[:username]) }
    puts members
    puts 'Fetching Duo seats...'
    seats = duo_seats
    puts ''
    puts seats
    out_of_sync = false
    out_of_sync = add_seats(members, seats) || out_of_sync
    out_of_sync = remove_seats(members, seats) || out_of_sync

    if out_of_sync
      return if @report_only || @force_sync

      raise 'Duo seats are out of sync. Run with FORCE_SYNC=1 to sync.'
    else
      puts 'Duo seats are in sync.'
    end
  end

  def add_seats(members, seats)
    seats_added = false
    members.each do |member|
      next if seats.any? { |seat| seat[:id] == member[:id] }

      seats_added = true
      puts "Adding #{member[:username]} to Duo seats"
      add_user(member[:id]) if @force_sync
    end

    seats_added
  end

  def remove_seats(members, seats)
    seats_removed = false
    seats.each do |seat|
      next if members.any? { |member| member[:id] == seat[:id] }

      seats_removed = true
      puts "Removing #{seat[:username]} from Duo seats"
      remove_user(seat[:id]) if @force_sync
    end

    seats_removed
  end
end

if $PROGRAM_NAME == __FILE__
  start = Time.now

  service = SyncDuoUsers.new
  service.execute

  puts '==='
  puts "Done in #{Time.now - start} seconds."
end
# rubocop:enable Metrics/ClassLength
